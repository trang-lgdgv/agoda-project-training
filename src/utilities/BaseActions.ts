import { action, gondola } from "gondolajs";
import Constant from "../data/Constant";

export class BaseActions{
    @action("Open web URL", "Navigate to home page")
    public navigateTo() {
        gondola.navigate(Constant.URL);
    }

    @action("click element", "Click element when element is clickable")
    public async clickElement(element: ILocator | string){
        await this.waitForElement(element);
        let capabilities = await gondola.getCapabilities();
        if(capabilities["browserName"] == "chrome"){
            await gondola.click(element);
        }
        if(capabilities["platformName"] == "android"){
            await gondola.tap(element);
        }
    }

    @action("send text", "Send text to element when element exists")
    protected async sendText(element: ILocator | string, text : string){
        await this.waitForElement(element);
        await gondola.enter(element, text);
    }

    @action("send text", "Send text to element when element exists")
    protected async checkControlExist(element: ILocator | string){
        await this.waitForElement(element);
        await gondola.checkControlExist(element);
    }

    @action("move mouse to control")
    protected async moveMouseToControl(element: ILocator | string){
        await this.waitForElement(element);
        await gondola.moveMouse(element);
    }

    @action("select item")
    protected async selectItem(list: string | ILocator, item: string){
        await this.waitForElement(list);
        await gondola.select(list, item);
    }

    @action("report message Bug")
    public async reportBug(message: string){
        await gondola.report("Bug: " + message);
    }

    @action("report message Debug")
    public async reportDebug(message: string){
        await gondola.report("Debug: " + message);
    }

    @action("Check message in popup", "")
    public async checkMsgDisplayed(message:string) {
        let popupText = await gondola.getPopupText();
        await gondola.checkEqual(popupText, message);
    }

    @action("Wait for element", "")
    protected async waitForElement(element:ILocator|string){
        gondola.waitForElement(element, Constant.SHORT_TIME_WAIT_ELEMENT);
    }

    @action("Wait for disappear", "")
    protected async waitForDisappear(element:ILocator|string){
        await gondola.waitForDisappear(element, Constant.MEDIUM_TIME_WAIT_ELEMENT);
    }
    
    @action("Wait for disappear", "")
    protected async getElementsAttribute(element:ILocator|string, attribute: string){
        await gondola.waitForElement(element, Constant.SHORT_TIME_WAIT_ELEMENT);
        await gondola.getElementsAttribute(element, attribute);
    }
}export default new BaseActions();