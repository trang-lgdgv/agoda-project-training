import { gondola } from "gondolajs";

export class utility {
    public randomNumber(min:number,max:number):number{
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public randomString():string{
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }

    public replaceSpaceToHtmlStyle (str: string) : string {
        return str.split(" ").join("\u00A0");
    }

    public async scrollToElement(ele: any, from: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        console.log(check + "scrollToElement");
        let bound = await gondola.getElementBounds(from);
        let eleX = bound.left;
        let eleY = bound.top;
        while (!check){
            gondola.swipeByCoordinates(eleX, eleY, toX, toY);
            check = await gondola.doesControlExist(ele);
            console.log(check + "while - scrollToElement");
        }
    }

    public async swipeUntilElementAppreared(ele: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        // console.log(check + " Appreared");
        let screenSize = await gondola.getDeviceScreenSize();
        let screenSizeX = (screenSize.width / 2);
        let screenSizeY = (screenSize.height / 2);
        while (!check){
            gondola.swipeByCoordinates(screenSizeX, screenSizeY, toX, toY);
            check = await gondola.doesControlExist(ele);
            // console.log(check + "while - Appreared");
        }
    }

    public async swipeUntilElementDisappreared(ele: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        // console.log(check + " Disappreared");
        let screenSize = await gondola.getDeviceScreenSize();
        let screenSizeX = (screenSize.width / 2);
        let screenSizeY = (screenSize.height / 2);
        while (check){
            gondola.swipeByCoordinates(screenSizeX, screenSizeY, toX, toY);
            check = await gondola.doesControlExist(ele);
            // console.log(check + "while - Disappreared");
        }
    }
}
export default new utility();
