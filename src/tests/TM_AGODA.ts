import { TestCase, TestModule, gondola } from "gondolajs";
import DashBoardPage from "../pages/gondola_test_site/DashBoardPage";
import { Place } from "../data/Place";
import { Guest } from "../data/Guest";
import SearchResultsPage from "../pages/gondola_test_site/SearchResultsPage";

TestModule("TM_AGODA");

let todayIn = new Date();
let inDate = DashBoardPage.getDateOfNextWeek(todayIn, 1);
let todayOut = new Date();
let outDate = DashBoardPage.getDateOfNextWeek(todayOut, 3);

let place = new Place("Da Nang", inDate, outDate);

let guest = new Guest("Family travelers", 2, 2, 2, 2, 13);

// TestCase("TC_AGODA_001, Search and sort hotel successfully", async () => {
//     await DashBoardPage.searchPlace(place, guest);
//     await SearchResultsPage.checkDisplayedCorrectData(place.destination);
//     await SearchResultsPage.sortLowestPrice();
// });

// TestCase("TC_AGODA_002, Search and filter hotel successfully", async () => {
//     let minPrice = 500000;
//     let maxPrice = 1000000;
//     let star = 3;
//     let isFilter = true;

//     await DashBoardPage.searchPlace(place, guest);
//     await SearchResultsPage.checkDisplayedCorrectData(place.destination);

//     await SearchResultsPage.clickFilterPrice();
//     await SearchResultsPage.filterPrice(minPrice, maxPrice);

//     await SearchResultsPage.clickFilterStar();
//     await SearchResultsPage.filterStarRaing(3);

//     await SearchResultsPage.checkHighlightedFilter();

//     await SearchResultsPage.checkDisplayedCorrectData(place.destination, isFilter, star, minPrice, maxPrice);
//     await SearchResultsPage.removePriceFilter();
// });

TestCase("TC_AGODA_003, Add hotel into Favourite successfully", async () => {    
    await DashBoardPage.searchPlace(place, guest);
    await SearchResultsPage.checkDisplayedCorrectData(place.destination);

    await SearchResultsPage.clickFilterMore();
    await SearchResultsPage.clickMoreMenuItem();
    await SearchResultsPage.clickFirstHotel();
});