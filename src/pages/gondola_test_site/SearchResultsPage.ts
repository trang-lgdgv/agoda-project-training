import { action, gondola, locator, page, KeyCode } from "gondolajs";
import { BaseActions } from "../../utilities/BaseActions";
import  utility  from "../../utilities/utility";
@page
export class SearchResultsPage extends BaseActions{
    @locator private areaCityName = async (index: number, destination?: string) => {
        return {
            xpath: "(//span[@class='areacity-name']/span[contains(text(), '" + destination + "')])[" + index.toString() + "]",
            android: "(//*[@resource-id='com.agoda.mobile.consumer:id/label_ssr_hotelname'])[" + index.toString() + "]",
        }
    };
    @locator private hotelName = async (name: string) => {
        return {
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/label_ssr_hotelname' and @text='" + name + "']"
        }
    };
    @locator private lowestPrice = { 
        xpath: "//a[@data-element-name='search-sort-price']",
        android: "//*[@text='Lowest price']/.."
    };
    @locator private bestMatchButton = {xpath: "//*[@text='Best match']/../.."}

    @locator public filterMenu = async (componentData?: string) => { 
        return {
            xpath: "//div[@data-component='" + componentData + "']/button",
            android: "//*[@text='My filter']/../.."
        }
    };
    @locator public moreMenuItem = { 
        xpath: "//i[@class='ficon ficon-non-smoking-room']/../../../../..",
        android: ""
    };
    @locator private doneButton = { 
        xpath: "//button[@class='btn MoreFilter__Button MoreFilter__Button--blue']",
        android: ""
    };
    @locator private firstHotelPanel = { 
        xpath: "(//div[@class='property-card-content'])[1]",
        android: ""
    };
    @locator private minPriceTextBox = { 
        xpath: "//input[@id='price_box_0']",
        android: ""
    };
    @locator private maxPriceTextBox = { 
        xpath: "//input[@id='price_box_1']",
        android: ""
    };
    @locator private starRatingMenuItem = async (numOfStar: number) => { 
        return {
            xpath: "//span[@class='filter-item-info StarRating-" + numOfStar.toString() + " ']",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/button_sortfilter_" + numOfStar.toString() + "star']"
        }
    };
    @locator private filterButton = { 
        xpath: "//*[@resource-id='com.agoda.mobile.consumer:id/button_sortfilter_done']",
        android: ""
    };
    @locator private clearFilterHyperlink = { 
        xpath: "//a[@class='filter-btn clear-filter-box']",
        android: ""
    };
    @locator private sliderPanel = async (sliderValue: number) => { 
        return {
            xpath: "//div[contains(@class, 'handle-" + sliderValue.toString() + "')]",
            android: ""
        }
    };
    @locator private starRatingIcon = async (index: number, destination?: string) => { 
        return {
            xpath: "(//span[@class='areacity-name']/span[contains(text(), '" + destination + "')]/../../..//span/i[@data-selenium='hotel-star-rating'])[" + index.toString() + "]",
            android: "(//*[@resource-id='com.agoda.mobile.consumer:id/rating_view'])["+ index +"]"
        }
    };

    @locator private actualPrice = (index: number) => { 
        return {
            xpath: "(//span[@class='price-box__price__amount'])["+ index +"]",
            android: "(//*[@resource-id='com.agoda.mobile.consumer:id/textView_search_hotel_price'])["+ index +"]"
        }
    };
    @locator private actualPriceText = (price: string) => { 
        return {
            xpath: "",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/textView_search_hotel_price' and @text='"+ price +"']"
        }
    };

    public async checkDisplayedCorrectData(destination:string, isFilter:boolean=false, starRatingFilter:number=1.0, minPrice:number=1.0, maxPrice:number=1.0) {
        let listdata:string [] = Array(5);
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            for (var i = 1; i <= listdata.length; i++) {
                let check = await gondola.doesControlExist(await this.areaCityName(i, destination));
                let areaCityName = await gondola.getText(await this.areaCityName(i, destination));
                if (check) {
                    console.log(i + ". Correctly - Destination: " + areaCityName);
                    gondola.scrollTo(await this.areaCityName(i, destination));
                }

                if (isFilter == true) {
                    let price = parseInt((await gondola.getText(this.actualPrice(i))).split(",").join(""));
                    if (price < minPrice || price > maxPrice) {
                        console.log("Incorrectly - Price");
                    } else {
                        console.log("Correctly - Price: " + minPrice + " <= " + price + " <= " + maxPrice);
                    }

                    let star = await gondola.getElementsAttribute(await this.starRatingIcon(i, destination), "aria-label");
                    let numOfStar = parseInt(star[0].replace(" star", ""));
                    if (numOfStar >= starRatingFilter) {
                        console.log("Correctly - Star: " + numOfStar + " >= " + starRatingFilter );
                    } else {
                        console.log("Incorrectly - Star");
                    }
                    gondola.scrollTo(await this.starRatingIcon(i, destination));
                }
            }   
        }

        if(capabilities.platformName == "android"){
            for (var i = 1; i <= listdata.length; i++) {
                await utility.swipeUntilElementAppreared(await this.areaCityName(i), 0, 400);
                console.log(listdata.length + ". Search Result is displayed correctly - Destination: " + destination);
                let hotelName = await gondola.getText(await this.areaCityName(i));
                await utility.swipeUntilElementDisappreared(await this.hotelName(hotelName), 0, -400);

                if (isFilter == true) {
                    let star = await gondola.getElementsAttribute(await this.starRatingIcon(i), "content-desc");
                    let numOfStar = parseInt(star[0]);
                    if (numOfStar >= starRatingFilter) {
                        console.log("Correctly - Star: " + numOfStar + " >= " + starRatingFilter );
                    } else {
                        console.log("Incorrectly - Star");
                    }
                }
                i = 0;
                listdata.length -= 1;
            }
        }   
    }

    public async sortLowestPrice() {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.platformName == "android"){
            gondola.tap(this.bestMatchButton);
        }
        await this.clickElement(this.lowestPrice);
        let index:number[] = Array(5);
        let listPrice:number[] = Array(5);
        var isSorted = true;

        if(capabilities.browserName == "chrome"){
            for (var i = 0; i < listPrice.length; i++) {
                listPrice[i] = parseInt((await gondola.getText(this.actualPrice(i+1))).replace(",", ""));
                if(listPrice[i] > listPrice[i+1]) {
                    isSorted = false;
                    break;
                }
                console.log(listPrice[i]);
            }
            console.log(isSorted);
        }
        if(capabilities.platformName == "android"){
            for (var i = 1; i <= index.length; i++) {
                await utility.swipeUntilElementAppreared(this.actualPrice(i), 0, 400);
                let price = await gondola.getText(this.actualPrice(i));
                listPrice.push(parseInt(price.replace(/(đ |¥ )/g, "").replace(",", "")));
                await utility.swipeUntilElementDisappreared(this.actualPriceText(price), 0, -400);
                i = 0;
                index.length -= 1;
            }
            for (let j = 0; j < listPrice.length; j++) {
                if(listPrice[j] > listPrice[j+1]) {
                    isSorted = false;
                    break;
                }
                console.log(listPrice[j]);  
            }
            console.log(isSorted);
        } 
    }

    public async clickFilterPrice() {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            await this.clickElement(await this.filterMenu("PriceFilterRange"));
        }
    }

    public async clickFilterMore() {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            await this.clickElement(await this.filterMenu("more"));
        }
    }

    public async clickFilterStar() {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            await this.clickElement(await this.filterMenu("StarRating"));
        }
        if(capabilities.platformName == "android"){
            await this.clickElement(await this.filterMenu());
        }
    }

    public async clickFirstHotel() {
        await this.clickElement(this.firstHotelPanel);
    }

    public async clickMoreMenuItem() {
        await this.clickElement(this.moreMenuItem);
        await this.clickElement(this.doneButton);
    }

    public async filterPrice(min: number, max: number) {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            await this.sendText(this.minPriceTextBox, min.toString());
            await this.sendText(this.maxPriceTextBox, max.toString());
            await gondola.pressKey(KeyCode.Enter);
        }
    }

    public async filterStarRaing(numOfStar: number) {
        await this.clickElement(await this.starRatingMenuItem(numOfStar));
        let capabilities = await gondola.getCapabilities();
        if(capabilities.platformName == "android"){
            gondola.tap(this.filterButton);
        }
    }

    public async checkHighlightedFilter() {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            let valueOfClass = await gondola.getElementsAttribute(await this.filterMenu("PriceFilterRange"), "class");
            if (valueOfClass[0].includes("PillDropdown__Button--select")) {
                console.log("OK... filtered is highlighted"); 
            } else {
                console.log("NO... filtered is not highlighted");
            }
            let valueOfClassStar = await gondola.getElementsAttribute(await this.filterMenu("StarRating"), "class");
            if (valueOfClassStar[0].includes("PillDropdown__Button--select")) {
                console.log("OK... filtered is highlighted"); 
            } else {
                console.log("NO... filtered is not highlighted");
            }
        }
        if(capabilities.platformName == "android"){
            let valueOfClass = await gondola.getElementsAttribute(await this.filterMenu(), "selected");
            if(valueOfClass){
                console.log("OK... filtered is highlighted"); 
            } else {
                console.log("NO... filtered is not highlighted");
            }
        }
    }

    public async removePriceFilter() {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            gondola.scrollTo(await this.filterMenu("PriceFilterRange"));
            await this.clickFilterPrice();
            await this.clickElement(this.clearFilterHyperlink);
            gondola.checkControlNotExist(this.clearFilterHyperlink);
    
            let minValue = await gondola.getElementsAttribute(await this.sliderPanel(1), "aria-valuenow");
            gondola.checkEqual(minValue[0], "0");
            let maxValue = await gondola.getElementsAttribute(await this.sliderPanel(2), "aria-valuenow");
            gondola.checkEqual(maxValue[0], "40");
            console.log(minValue + "...." + maxValue);
        }
    }
}
export default new SearchResultsPage();
