import { action, gondola, locator, page } from "gondolajs";
import { BaseActions } from "../../utilities/BaseActions";
import { Place } from "../../data/Place";
import  utility  from "../../utilities/utility";
import { Guest } from "../../data/Guest";
@page
export class DashBoardPage extends BaseActions{
    @locator private searchContainer = { 
        xpath: "//div[@class='SearchContainer__WithPackageDeals']",
        android: ""
    };
    @locator private destinationTextBox = { 
        xpath: "//input[@class='SearchBoxTextEditor SearchBoxTextEditor--autocomplete']",
        android: "//*[@resource-id='com.agoda.mobile.consumer:id/textbox_textsearch_searchbox']"
    };
    @locator private autocompleteList = { 
        xpath: "//ul[@class='AutocompleteList']/li",
        android: "//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout"
    };
    @locator private searchButton = { 
        xpath: "//span[@class='Searchbox__searchButton__text']",
        android: "//*[@resource-id='com.agoda.mobile.consumer:id/button_home_search']"
    };
    @locator private getAutocompleteList = { 
        xpath: "//ul[@class='AutocompleteList']/li[1]",
        android: "//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]",
    };
    @locator private numberMenuItem = async (occupancy: string) => { 
        return {
            xpath: "//div[contains(@data-selenium, '" + occupancy + "')]/span[2]",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/" + occupancy.toLowerCase() + "_selector']//*[@resource-id='com.agoda.mobile.consumer:id/amount']"
        }
    };
    @locator private minusMenuItem = async (occupancy: string) => { 
        return {
            xpath: "//div[contains(@data-selenium, '" + occupancy + "')]/span[1]",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/" + occupancy.toLowerCase() + "_selector']//*[@resource-id='com.agoda.mobile.consumer:id/minus']"
        }
    };
    @locator private plusMenuItem = async (occupancy: string) => { 
        return {
            xpath: "//div[contains(@data-selenium, '" + occupancy + "')]/span[4]",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/" + occupancy.toLowerCase() + "_selector']//*[@resource-id='com.agoda.mobile.consumer:id/plus']"
        }
    };
    @locator private travelerMenuItem = async (traveler: string) => { 
        return {
            xpath: "//div[.='" + traveler + "']",
            android: ""
        }
    };
    @locator private childAgeDropdownMenu = async (index: number) => { 
        return {
            xpath: "//ul[@class='ChildAges__dropdown']/li[" + index.toString() + "]",
            android: "//*[@text ='Child " + index.toString() + "']/..//*[contains(@resource-id, 'spinner_child_age')]"
        }
    };
    @locator private childAgeDropdownMenuItemList = async (indexChild: number, indexChildList: number) => { 
        return {
            xpath: "//ul[@class='ChildAges__dropdown']/li[" + indexChild.toString() + "]//option[@value='" + indexChildList.toString() + "']",
            android: "//android.widget.TextView[@text='" + indexChildList.toString() + "']"
        }
    };
    @locator private getDatePickerWithCaption = async (caption: string, date: number) => { 
        return {
            xpath: "//div[@class='DayPicker-Caption']/div[.='" + caption + "']/../..//span[.='" + date + "']",
            android: "//android.widget.TextView[@content-desc='" + date + " " + caption + "']"
        }
    };

    @locator private gotItButton = { id: "com.agoda.mobile.consumer:id/got_it"};
    @locator private allowButton = { id: "com.android.packageinstaller:id/permission_allow_button"};
    @locator private allowAlwayButton = { id: "com.android.permissioncontroller:id/permission_allow_always_button"};
    @locator private notNowButton = { xpath: "//*[@text = 'Not now']"};
    
    @locator private homeSearchTextBox = { id: "com.agoda.mobile.consumer:id/textbox_home_textsearch"};
    @locator private datepickerPanel = { id: "com.agoda.mobile.consumer:id/panel_home_datepicker"};
    @locator private doneButton = { id: "com.agoda.mobile.consumer:id/button_datepicker_done"};
    @locator private occupancyPanel = { id: "com.agoda.mobile.consumer:id/panel_home_occupancy"};
    @locator private preferFamilyRoomCheckBox = { id: "com.agoda.mobile.consumer:id/preferFamilyRoomCheckBox"};
    @locator private occupancyconfirmButton = { id: "com.agoda.mobile.consumer:id/occupancy_confirm"};

    public async searchPlace(place: Place, guest: Guest) {
        let capabilities = await gondola.getCapabilities();
        if(capabilities.browserName == "chrome"){
            this.navigateTo();
            this.checkControlExist(this.searchContainer);
        }
        if(capabilities.platformName == "android"){
            gondola.tap(this.gotItButton);
            let check = await gondola.doesControlExist(this.allowButton);
            if (check) {
                gondola.tap(this.allowButton);
            }
            let check0 = await gondola.doesControlExist(this.allowAlwayButton);
            if (check0) {
                gondola.tap(this.allowAlwayButton);
            }
            let check1 = await gondola.doesControlExist(this.notNowButton);
            if (check1) {
                gondola.tap(this.notNowButton);
            }
            gondola.tap(this.homeSearchTextBox);
        }
        await this.selectDestination(place.destination);
        if(capabilities.platformName == "android"){
            gondola.tap(this.datepickerPanel);
        }
        await this.selectDate(place.inDate);
        await this.selectDate(place.outDate);
        if(capabilities.platformName == "android"){
            gondola.tap(this.doneButton);
            let check = await gondola.doesControlExist(this.occupancyPanel);
            if (check) {
                gondola.tap(this.occupancyPanel);
            }
        }
        if(capabilities.browserName == "chrome"){
            await this.clickElement(await this.travelerMenuItem(guest.traveler));
        }
        await this.selectGuestDetails(guest);
        if(capabilities.platformName == "android"){
            gondola.tap(this.preferFamilyRoomCheckBox);
            gondola.tap(this.occupancyconfirmButton);
        }
        await this.clickElement(this.searchButton);
    }

    private async selectDestination(destination:string) {
        await this.sendText(this.destinationTextBox, destination); 
        await this.clickElement(this.getAutocompleteList); 
    }

    private async selectGuestDetails(guest: Guest) {
        let numOfRooms = parseInt(await gondola.getText(await this.numberMenuItem("Room")));
        while (numOfRooms != guest.rooms) {
            if (numOfRooms < guest.rooms) {
                await this.clickElement(await this.plusMenuItem("Room"));
            } else {
                await this.clickElement(await this.minusMenuItem("Room"));
            }
            numOfRooms = parseInt(await gondola.getText(await this.numberMenuItem("Room")));
        }   
        let numOfAdults = parseInt(await gondola.getText(await this.numberMenuItem("Adult")));
        while (numOfAdults != guest.adults) {
            if (numOfAdults < guest.adults) {
                await this.clickElement(await this.plusMenuItem("Adult"));
            } else {
                await this.clickElement(await this.minusMenuItem("Adult"));
            }
            numOfAdults = parseInt(await gondola.getText(await this.numberMenuItem("Adult")));
        }
        let numOfChildren = parseInt(await gondola.getText(await this.numberMenuItem("Children")));
        while (numOfChildren != guest.child) {
            if (numOfChildren < guest.child) {
                await this.clickElement(await this.plusMenuItem("Children"));
            } else {
                await this.clickElement(await this.minusMenuItem("Children"));
            }
            numOfChildren = parseInt(await gondola.getText(await this.numberMenuItem("Children")));
        }   
        let capabilities = await gondola.getCapabilities();
        for (let i = 0; i < guest.arr.length; i++) {
            await this.clickElement(await this.childAgeDropdownMenu(i+1));
            if(capabilities.platformName == "android"){
                await utility.scrollToElement(await this.childAgeDropdownMenuItemList(i+1, guest.arr[i]), await this.childAgeDropdownMenuItemList(i+1, 11), 0, 500);
            } 
            await this.clickElement(await this.childAgeDropdownMenuItemList(i+1, guest.arr[i]));
        }
    }

    private async selectDate(date: Date) {
        let monthNames = ["January", "February", "March", "April", "May", "June", 
            "July", "August", "September", "October", "November", "December"];
        let caption = monthNames[date.getMonth()] + " " + date.getFullYear().toString();
        await this.clickElement(await this.getDatePickerWithCaption(caption, date.getDate()));
    }

    public getDateOfNextWeek(currentDate: Date, dayOfWeek: number) {
        currentDate.setDate(currentDate.getDate() + (dayOfWeek + 7 - currentDate.getDay()));
        return currentDate;
    }

}
export default new DashBoardPage();
