export class Constant{
    //Time wait
    public readonly SHORT_TIME_WAIT_ELEMENT: number = 15;
    public readonly MEDIUM_TIME_WAIT_ELEMENT: number = 10;
    public readonly LONG_TIME_WAIT_ELEMENT: number = 15; 
    //Url
    public readonly URL: string = "https://www.agoda.com";
} export default new Constant();