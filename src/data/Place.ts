export class Place{
    destination:string;
    inDate: Date;
    outDate: Date;
    constructor(destination: string, inDate: Date, outDate: Date){
        this.destination = destination;
        this.inDate = inDate;
        this.outDate = outDate;
    }
}