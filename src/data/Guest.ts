export class Guest{
    traveler:string;
    rooms:number;
    adults:number;
    child:number;
    arr:number[] = [];
    constructor(traveler:string, rooms:number, adults:number, child:number, ...items: number[]){
        this.traveler = traveler;
        this.rooms = rooms;
        this.adults = adults;
        this.child = child;
        this.arr.push(...items);
    }
}